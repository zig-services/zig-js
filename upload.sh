#!/bin/bash

set -e

VERSION=${1:-$(node -p 'require("./package.json").version')}

if [[ $VERSION == *beta* ]] ; then
    SUFFIX=${1:-dev}
    CACHE_CONTROL="private, no-store, no-cache"
else
    if aws s3 ls s3://lib.zig.services/zig/$VERSION/libzig.js &> /dev/null ; then
        echo "Error: version $VERSION already exists on s3 storage."
        exit 1
    fi

    SUFFIX=stable
    CACHE_CONTROL="public, max-age=60"
fi

VERSION_MAJOR=$(node -p "require('semver').major('$VERSION')")-$SUFFIX
VERSION_MAJOR_MINOR=$(node -p "v=require('semver'); v.major('$VERSION') + '.' + v.minor('$VERSION')")-$SUFFIX

echo "Uploading in version '$VERSION_MAJOR'"
for FILE in dist/bundles/*.js ; do
  aws s3 cp \
      --acl=public-read \
      --content-type="application/javascript; charset=utf8" \
      --cache-control="$CACHE_CONTROL" \
       $FILE s3://lib.zig.services/zig/$VERSION_MAJOR/
done

#echo "Uploading in version '$VERSION_MAJOR_MINOR'"
#for FILE in dist/bundles/*.js ; do
#  aws s3 cp \
#      --acl=public-read \
#      --content-type="application/javascript; charset=utf8" \
#      --cache-control="$CACHE_CONTROL" \
#       $FILE s3://lib.zig.services/zig/$VERSION_MAJOR_MINOR/
#  done
#
#echo "Uploading in version '$VERSION'"
#for FILE in dist/bundles/*.js ; do
#  aws s3 cp \
#      --acl=public-read \
#      --content-type="application/javascript; charset=utf8" \
#      --cache-control="$CACHE_CONTROL" \
#       $FILE s3://lib.zig.services/zig/$VERSION/
#done

