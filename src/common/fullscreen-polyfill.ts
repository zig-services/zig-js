const document = window.document as any;

const spec: any = {
  'fullscreen': 'fullscreen',
  'fullscreenEnabled': 'fullscreenEnabled',
  'fullscreenElement': 'fullscreenElement',
  'fullscreenchange': 'fullscreenchange',
  'fullscreenerror': 'fullscreenerror',
  'exitFullscreen': 'exitFullscreen',
  'requestFullscreen': 'requestFullscreen'
}

const webkit: any = {
  'fullscreen': 'webkitIsFullScreen',
  'fullscreenEnabled': 'webkitFullscreenEnabled',
  'fullscreenElement': 'webkitFullscreenElement',
  'fullscreenchange': 'webkitfullscreenchange',
  'fullscreenerror': 'webkitfullscreenerror',
  'exitFullscreen': 'webkitExitFullscreen',
  'requestFullscreen': 'webkitRequestFullscreen'
}

const moz: any = {
  'fullscreen': 'mozFullScreen',
  'fullscreenEnabled': 'mozFullScreenEnabled',
  'fullscreenElement': 'mozFullScreenElement',
  'fullscreenchange': 'mozfullscreenchange',
  'fullscreenerror': 'mozfullscreenerror',
  'exitFullscreen': 'mozCancelFullScreen',
  'requestFullscreen': 'mozRequestFullScreen'
}

const ms: any = {
  'fullscreen': '',
  'fullscreenEnabled': 'msFullscreenEnabled',
  'fullscreenElement': 'msFullscreenElement',
  'fullscreenchange': 'MSFullscreenChange',
  'fullscreenerror': 'MSFullscreenError',
  'exitFullscreen': 'msExitFullscreen',
  'requestFullscreen': 'msRequestFullscreen'
}

// Get the vendor fullscreen prefixed api
const fsVendorKeywords = (function getFullscreenApi() {
  const fullscreenEnabled = [
    spec.fullscreenEnabled,
    webkit.fullscreenEnabled,
    moz.fullscreenEnabled,
    ms.fullscreenEnabled].find((prefix) => document[prefix])

  return [spec, webkit, moz, ms].find((vendor) => {
    return Object.values(vendor).find((prefix) => prefix === fullscreenEnabled)
  }) || {};
})()

function handleEvent(eventType: any, event: any) {
  document[spec.fullscreen] = document[fsVendorKeywords.fullscreen]
    || !!document[fsVendorKeywords.fullscreenElement]
    || false;

  document[spec.fullscreenEnabled] = document[fsVendorKeywords.fullscreenEnabled] || false
  document[spec.fullscreenElement] = document[fsVendorKeywords.fullscreenElement] || null
  document.dispatchEvent(new Event(eventType), event.target)
}

function setupShim() {
  // fullscreen
  // Defaults to false for cases like MS where they do not have this
  // attribute. Another way to check whether fullscreen is active is to look
  // at the fullscreenElement attribute.
  document[spec.fullscreen] = document[fsVendorKeywords.fullscreen]
    || !!document[fsVendorKeywords.fullscreenElement]
    || false

  // fullscreenEnabled
  document[spec.fullscreenEnabled] = document[fsVendorKeywords.fullscreenEnabled] || false

  // fullscreenElement
  document[spec.fullscreenElement] = document[fsVendorKeywords.fullscreenElement] || null

  // onfullscreenchange
  document.addEventListener(fsVendorKeywords.fullscreenchange, handleEvent.bind(document, spec.fullscreenchange), false)

  // onfullscreenerror
  document.addEventListener(fsVendorKeywords.fullscreenerror, handleEvent.bind(document, spec.fullscreenerror), false)

  // exitFullscreen
  document[spec.exitFullscreen] = function () {
    return document[fsVendorKeywords.exitFullscreen]?.()
  }

  // requestFullscreen
  const Element = window.Element as any;
  Element.prototype[spec.requestFullscreen] = function () {
    return this[fsVendorKeywords.requestFullscreen]?.apply(this, arguments)
  }
}

// Don't polyfill if it already exist
export default (typeof document[spec.fullscreenEnabled] !== 'undefined' || !fsVendorKeywords) ? {} : setupShim()
