import '../common/polyfills';

import {
    appendGameConfigToURL,
    GameConfig,
    GameMessageInterface,
    GameSettings,
    License,
    Logger,
    MessageClient,
    onDOMLoad,
    Options,
    parseGameConfigFromURL,
    sleep,
} from '../common';

import wrapperStyleCSS from './style.css';

import logoMGA from './malta.png';


const log = Logger.get('zig.wrapper');

const licenseMGA = 'This game is licensed by the Malta Gaming Authortiy';

/**
 * Get game config from window
 */
const GameSettings = (<any>window).GameSettings as GameSettings;
if (GameSettings == null) {
    throw new Error('window.GameConfig must be initialized.');
}

function licenseInformation(config: GameConfig): License {
    if (config.licenseToShow) {
        return config.licenseToShow;
    }

    const operator = guessOperatorId();

    if (operator === 'mylotto24') {
        return 'none';
    }

    if (operator === 'ilbg') {
        return 'none';
    }

    // all other are currently mga licensed.
    return 'mga';
}

/**
 * Create and load the real game inside the iframe.
 * This method will add the iframe to the body of the page.
 */
async function initializeGame(): Promise<HTMLIFrameElement> {
    const config: GameConfig = parseGameConfigFromURL();

    let url = appendGameConfigToURL(GameSettings.index || 'inner.html', config);

    // legacy games need extra patching. We'll need to inform the inner.html about that.
    if (GameSettings.legacyGame === true) {
        url += '&legacyGame=true';
    }

    log.info(`Creating iframe with URL ${url}`);

    // create iframe and insert into document.
    const iframe = document.createElement('iframe');
    iframe.src = url;
    iframe.allowFullscreen = true;
    iframe.scrolling = 'no';
    (iframe as any).allow = 'autoplay';

    const parentMessageClient = new MessageClient(window.parent);

    // forward errors to parent frame.
    iframe.onerror = err => parentMessageClient.sendError(err);

    // send the new config to the parent so it can update the frame size
    const outerMessageClient = new GameMessageInterface(parentMessageClient, config.canonicalGameName);
    outerMessageClient.updateGameSettings(GameSettings);

    // add game to window
    document.body.appendChild(iframe);

    const license = licenseInformation(config);
    if (!GameSettings.chromeless && !GameSettings.disableSplashScreen) {
        if (license === 'mga') {
            const splash = new SplashScreenController();
            document.body.appendChild(splash.$element);
        }
    }

    async function trySetupMessageClient(): Promise<void> {
        // wait for the content window to load.
        while (iframe.contentWindow == null) {
            log.info('contentWindow not yet available, waiting...');
            await sleep(250);
        }

        // initialize the message client to the game window
        const innerMessageClient = new MessageClient(iframe.contentWindow);

        // and proxy all messages between the frames
        proxyMessages(parentMessageClient, innerMessageClient);

        window.onfocus = () => {
            log.debug('Got focus, focusing iframe now.');
            setTimeout(() => {
                const contentWindow = iframe.contentWindow;
                if (contentWindow != null) {
                    contentWindow.focus();
                }
            });
        };
    }

    await trySetupMessageClient();

    return iframe;
}

function guessOperatorId(): string | null {
    // extract the "mylotto24" part of "mylotto24.frontends.zig.services"
    const match = /[^.]+/.exec(location.hostname);
    if (!match) {
        log.warn('Could not extract operatorId from ', location.hostname);
    }

    return match ? match[0] : null;
}

/**
 * Set up a proxy for post messages. Everything coming from the iframe will be forwarded
 * to the parent, and everything coming from the parent will be send to the iframe.
 */
function proxyMessages(parentMessageClient: MessageClient, innerMessageClient: MessageClient): void {
    innerMessageClient.register(ev => {
        log.debug('Proxy message parent <- game');
        parentMessageClient.send(ev);
    });

    parentMessageClient.register(ev => {
        log.debug('Proxy message parent -> game');
        innerMessageClient.send(ev);
    });
}

class SplashScreenController {
    public readonly $element = document.createElement('div');

    constructor() {
        this.$element.className = 'zig-splash';
        this.$element.innerHTML = `
            <div>
                <div>
                    <img style='width: 10em; margin-top: 0.5em;' src='${logoMGA}'>
                </div>
                <div style='margin-top: 0.5em'>
                    <small style="color: #ddd;">${licenseMGA}</small>
                </div>
            </div>
        `;

        setTimeout(() => this.fadeOut(), 1000);
    }

    private fadeOut() {
        this.$element.style.opacity = '0';
        setTimeout(() => this.remove(), 300);
    }

    private remove() {
        const parent = this.$element.parentElement;
        if (parent != null) {
            parent.removeChild(this.$element);
        }
    }
}

function initializeWinningClassOverride(): boolean {
    const override = Options.winningClassOverride;
    if (override == null)
        return false;

    const params = `&wc=${override.winningClass}&scenario=${override.scenarioId}`;
    if (location.href.indexOf(params) === -1) {
        const search = location.search.replace(/\b(scenario|wc)=[^&]+/g, '');
        const sep = location.search || '&';

        log.info('Replace outer.html with updated url:', search + sep + params);
        location.search = search + sep + params;

        return true;
    }

    return false;
}

onDOMLoad(() => {
    log.info(`Initializing zig wrapper`);

    // if we have a winning class override, we'll change the url and
    // reload the script. we wont be initializing the rest of the game here.
    if (initializeWinningClassOverride()) {
        return;
    }

    // inject the css style for the wrapper into the document
    const link = document.createElement('link');
    link.href = wrapperStyleCSS;
    link.rel = 'stylesheet';
    document.head.appendChild(link);

    void initializeGame();

    if (Options.debuggingLayer) {
        log.debug('Debugging options are set, showing debugging layer now.');

        const el = document.createElement('div');
        const winningClassOverride = {
            ...Options.winningClassOverride,
            scenario: Options.winningClassOverride?.scenario ? 'OVERRIDDEN' : 'ORIGINAL'
        };
        el.innerHTML = `
            <div style='position: absolute; top: 0; left: 0; font-size: 0.6em; padding: 0.25em; background: rgba(0, 0, 0, 128); color: white; z-index: 100;'>
                <strong>ZIG</strong> &nbsp;&nbsp;
                logging: ${Options.logging},
                locale: ${Options.localeOverride},
                wc override: ${JSON.stringify(winningClassOverride)}
            </div>`;

        document.body.appendChild(el.firstElementChild!);
    }
});
